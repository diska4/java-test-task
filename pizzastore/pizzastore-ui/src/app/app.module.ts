import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { CoreModule } from './@core/core.module';
import { BrowserModule } from '@angular/platform-browser';
import { NortalComponent } from './home/devaki/IdeaProjects/nortal/nortal.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    NortalComponent
  ],
  imports: [
    CoreModule,
    BrowserModule,
    RouterModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
