import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {AuthService} from "../auth.service";
import {first} from "rxjs/operators";
import {AlertService} from "../../@shared/alert/alert.service";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit{
  usr: string;


  registrationForm = this.formBuilder.group({
    username: new FormControl('', Validators.required),
    password: new FormControl('', [Validators.required
      /*Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}')*/]) // source of regex https://stackoverflow.com/questions/48350506/how-to-validate-password-strength-with-angular-5-validator-pattern
  })


  constructor(private formBuilder: FormBuilder,
              private redirect: Router,
              private http: HttpClient,
              private authSer: AuthService,
              private alertService: AlertService) {
  }
  registrationForm: FormGroup;
  ngOnInit() {}


  onSubmit() {
    // TODO Register user
    if (this.registrationForm.invalid) {
      return;
    }
    this.authSer.register(
      this.registrationForm.value.username,
      this.registrationForm.value.password)
      .pipe(first())
      .subscribe(
      () => {console.log("good job")},
      error => {
        if (error.status === 403) {

          this.alertService.error(error.error.message);
          }
        }
      );
    console.log(this.registrationForm.value.username);
    /*this.http.post('http://localhost:8080/auth/register',
      this.registrationForm.value, {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    }).subscribe(resp => console.log(resp))*/
    /*this.redirect.navigate(['/new-order']);*/
  }
}
