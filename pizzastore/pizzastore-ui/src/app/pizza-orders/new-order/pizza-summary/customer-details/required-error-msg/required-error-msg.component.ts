import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-required-error-msg',
  templateUrl: './required-error-msg.component.html',
  styleUrls: ['./required-error-msg.component.css']
})
export class RequiredErrorMsgComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
