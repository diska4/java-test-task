import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequiredErrorMsgComponent } from './required-error-msg.component';

describe('RequiredErrorMsgComponent', () => {
  let component: RequiredErrorMsgComponent;
  let fixture: ComponentFixture<RequiredErrorMsgComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequiredErrorMsgComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequiredErrorMsgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
