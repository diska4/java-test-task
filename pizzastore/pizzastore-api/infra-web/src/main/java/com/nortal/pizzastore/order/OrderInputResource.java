package com.nortal.pizzastore.order;

import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@ToString
class OrderInputResource {
  public CustomerResource customer = new CustomerResource();
  public List<Pizza> pizzas = new ArrayList<>();
  @ToString
  static class Pizza {

    public Long base;
    public List<Long> toppings;
  }
}
