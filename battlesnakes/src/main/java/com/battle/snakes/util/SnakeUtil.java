package com.battle.snakes.util;


import com.battle.snakes.game.*;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Random;

public class SnakeUtil {

    private static final Random RANDOM = new Random();

    public static MoveType getRandomMove(List<MoveType> possibleMoves) {
      /* TODO
       * Given all possible moves, picks a random move
       * */

      return possibleMoves != null && possibleMoves.size() != 0 ? possibleMoves.get(RANDOM.nextInt(possibleMoves.size())) : null;
    }

    public static boolean isInBounds(Board board, Coordinate coordinate) {
      /* TODO
       * Given the game board, calculates if a coordinate is within the board
       * */
        return board.getWidth() >= coordinate.getX() && board.getHeight() >= coordinate.getY();
    }

    public static Coordinate getNextMoveCoords(MoveType moveType, Coordinate start) {
      /* TODO
       * Given the move type and the start coordinate, returns the coordinates of the next move
       * */
      switch (moveType){
          case LEFT: return Coordinate.builder().x(start.getX()-1).y(start.getY()).build();
          case RIGHT: return Coordinate.builder().x(start.getX()+1).y(start.getY()).build();

          case UP: return Coordinate.builder().x(start.getX()).y(start.getY()-1).build();
          case DOWN: return Coordinate.builder().x(start.getX()).y(start.getY()+1).build();
          default: return Coordinate.builder().x(0).y(0).build();
      }
    }

    public static List<MoveType> getAllowedMoves(MoveRequest request) {
        Board board = request.getBoard();
        Coordinate snakeHead = request.getYou().getBody().get(0);
      /* TODO
       * Given the move request, returns a list of all the moves that do not end in the snake dieing
       * Hint: finding all the coordinates leading to the snakes death and
       * comparing it to the potential moves is a good starting point
       * */
      return Collections.emptyList();
    }

    public static double getDistance(Coordinate first, Coordinate second) {
      /* TODO
       * Given two coordinates on a 2D grid, calculates the distance between them
       * */
      double xCoordinatesResult = Math.pow((first.getX() - second.getX()), 2);
      double yCoordinatesResult = Math.pow((first.getY() - second.getY()), 2);
      return Math.pow((xCoordinatesResult + yCoordinatesResult), 0.5);
    }

    public static MoveType getNearestMoveToTarget(Coordinate target, Coordinate current, List<MoveType> moves) {
      /* TODO
       * Given the target coordinate, the current coordinate and a list of moves, returns
       * the nearest move to the target, selected from the moves list
       * */
      if (target.getX() > current.getX() && moves.contains(MoveType.RIGHT)) return MoveType.RIGHT; // is located on left side from current position
      if(target.getX() < current.getX() && moves.contains(MoveType.LEFT)) return MoveType.LEFT; // is located on right side from current position
      if (target.getY() > current.getY() && moves.contains(MoveType.DOWN)) return MoveType.DOWN; // is located on bottom from current position
      if (target.getY() < current.getY() && moves.contains(MoveType.UP)) return MoveType.UP; // is located on top from current position
      return null;
    }

    public static Coordinate getNearestCoordinateToTarget(Coordinate target, List<Coordinate> coords) {
      /* TODO
       * Given the target coordinate and a list of coordinates, finds the nearest coordinate to the target
       * */
      return Collections.min(coords, Comparator.comparing(coord -> getDistance(target, coord)));
    }
}
